#!/usr/bin/env python

from csv import DictWriter
from StringIO import StringIO
import re
from flask import Flask, request, make_response, abort, json
from flask.ext.pymongo import PyMongo

app = Flask(__name__)
mongo = PyMongo(app)

@app.route('/save/<experiment>', methods=['POST'])
def save_data(experiment):
    data = {}
    for k,v in request.form.iteritems():
        # &#46; is HTML escape for period. MongoDB can't handle periods in keys
        k = re.sub(r'\.', '&#46;', k)
        # &#36; is escape for $. MongoDB can't handle keys starting with $
        k = re.sub(r'^\$', '&#36;', k)
        # key can't be "_id"
        # TODO: catch that
        data[k] = v
    
    mongo.db.experiments[experiment].insert(data)
    return "OK Then."

@app.route('/get/<experiment>/<fileformat>', methods=['GET'])
def get_data(experiment, fileformat):
    if fileformat not in ('csv','json'):
        abort(400)
    else:
        # You have to map-reduce to get the list of keys
        #http://stackoverflow.com/a/2308036
        mongo.db.experiments[experiment].map_reduce('function() { for (var key in this) { emit(key, null); }}',
            'function(key, stuff) { return null; }',
            experiment + '_keylist')
        keylist = mongo.db[experiment + '_keylist'].distinct("_id")
        keylist.remove('_id')
        escaped_keys = []
        for k in keylist:
            k = re.sub(r'&#46;', '.', k)
            k = re.sub(r'^&#36;', '$', k)
            escaped_keys.append(k)

        results = mongo.db.experiments[experiment].find()

        if fileformat == 'csv':
            csvout = StringIO()
            dw = DictWriter(csvout, keylist)
            # Header row with the escaped key names
            dw.writerow(dict(zip(keylist, escaped_keys)))
            for row in results:
                row.pop('_id')
                dw.writerow(row)

            response = make_response(csvout.getvalue())
            response.headers['Content-Disposition'] = 'attachment; filename=' + experiment + '.csv'
            return response

        if fileformat == 'json':
            dot = re.compile(r'&#46;')
            dollar = re.compile(r'^&#36;');
            cleaned_results = []
            for row in results:
                row.pop('_id')
                for k in row.iterkeys():
                    if dot.search(k):
                        row[re.sub(r'&#46;', '.', k)] = row.pop(k)
                    if dollar.search(k):
                        row[re.sub(r'^&#36;', '$', k)] = row.pop(k)
                cleaned_results.append(row)
            response = make_response(json.dumps(cleaned_results, sort_keys=True, indent=4))
            response.headers['Content-Type'] = 'application/json'
            return response

        return "OK"

if __name__ == '__main__':
    app.run(debug=True)
